process.env.NODE_ENV = 'development'

const webpack = require('webpack');
const config = require('./webpack.config')

config.devtool = 'eval-source-map'
config.mode = 'development'
config.plugins = [
	new webpack.HotModuleReplacementPlugin()
]

module.exports = config
