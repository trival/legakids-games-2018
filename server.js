var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.develop');

const options = {
  publicPath: config.output.publicPath,
  hot: true,
  host: 'localhost',
  historyApiFallback: true,
  contentBase: './dist/'
}

WebpackDevServer.addDevServerEntrypoints(config, options);

new WebpackDevServer(webpack(config), options)
.listen(3000, 'localhost', function (err, result) {
  if (err) {
    console.log(err);
  }

  console.log('Listening at localhost:3000');
});
