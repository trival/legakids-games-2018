import './numbersSetup'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { observable } from 'mobx'
import { App } from './components/App'
import { appState } from './state/app'
import { normalize, setupPage } from 'csstips'
import { levelState } from './state/level'

normalize();
setupPage('#root');


ReactDOM.render(<App />, document.getElementById('root'))
  ; (window as any).state = observable({ appState, levelState })


if ((module as any).hot) {
  (module as any).hot.accept()
}
