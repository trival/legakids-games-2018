import * as React from 'react';
import { style } from 'typestyle/lib';
import * as css from 'csstips'
import { fullAbsolute, colorInfoBackground, colorFontBlue, gpuTransform } from '../styles/shared';
import { screenTransitionDuration } from '../../state/app';


interface Props {
	title?: string
}


export const InfoScreen: React.StatelessComponent<Props> = ({ children, title }) =>
		<div className={container}>
			<article className={infoScreenClass}>
				<header>
					<h1>{title}</h1>
				</header>
				{children}
			</article>
		</div>


const transitionSeconds = screenTransitionDuration / 1000


const container = style(gpuTransform, fullAbsolute, css.vertical, {
	overflow: 'auto',
	backgroundColor: 'rgba(4, 0, 10, 0.55)',
	transition: `background-color ${transitionSeconds}s`,
	padding: '2em 0 2em',
	$nest: {
		'&.screen-enter, &.screen-exit': {
			backgroundColor: 'rgba(0, 0, 0, 0)'
		},
		'&.screen-enter>article, &.screen-exit>article': {
			opacity: 0,
			transform: 'translate(0, -200%)'
		},
	}
})


const infoScreenClass = style(css.selfCenter, css.content, {
	position: 'relative',
	padding: '6em 2em 1em',
	margin: 'auto',
	width: '35em',
	maxWidth: '90%',

	backgroundColor: colorInfoBackground,
	borderRadius: '1em',
	boxShadow: '0 0.7em 1.5em rgba(0, 0, 0, 0.4)',
	transform: 'translate(0, 0)',
	opacity: 1,
	transition: `opacity ${transitionSeconds}s, transform ${transitionSeconds}s`,

	$nest: {
		'header': {
			position: 'absolute',
			top: '0',
			left: '1em',
			right: '1em',
			width: 'calc(100% - 2em)',
			height: '6em',
			background: 'url(img/banner_mitte.png)',
			color: colorFontBlue,
			backgroundSize: '100% 100%'
		},

		'header>h1': {
			margin: '0.45em 0'
		},

		'header::before, header::after': {
			content: '" "',
			display: 'block',
			position: 'absolute',
			top: 0,
			width: '6em',
			height: '6em',
			backgroundRepeat: 'no-repeat',
			backgroundSize: '100% 100%',
			zIndex: -1
		},

		'header::after': {
			right: '-3.5em',
			backgroundImage: 'url(img/banner_rechts.png)'
		},
		'header::before': {
			left: '-3.5em',
			backgroundImage: 'url(img/banner_links.png)'
		},
		'& nav': {
			margin: '2em -1.4em -0.4em',
		},
		'& header+nav': {
			marginTop: '1em'
		},

		'& footer': {
			position: 'relative',
			margin: '-2em -2em -1em',
			zIndex: -1
		},
		'& footer img': {
			width: '100%',
			display: 'block',
			verticalAlign: 'bottom'
		},

		'input': {
			padding: '0.2em 1em',
			fontSize: '1.2em',
			fontFamily: 'Amble Regular',
			'-webkit-user-select': 'auto',
			userSelect: 'auto'
		}
	}
})
