import * as React from 'react';
import { gameData } from '../../data/config';
import { startTask } from '../../state/app';
import { screenBase } from '../styles/shared';
import { style } from 'typestyle/lib';
import { InfoScreen } from '../shared/InfoScreen'


export const Menu: React.StatelessComponent = () =>
	<InfoScreen title="Wähle Dein Spiel">
		<nav className={menuClass}>
			{gameData.tasks.map((t, i) => ([
				<button onClick={() => startTask(t)} key={'task' + i}>{t.label}</button>,
				<br key={'br' + i}/>
			]))}
		</nav>
	</InfoScreen>


const menuClass = style(screenBase)
