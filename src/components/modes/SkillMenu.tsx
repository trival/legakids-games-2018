import * as React from 'react';
import { appState, chooseSkillLevel } from '../../state/app';
import { screenBase } from '../styles/shared';
import { style } from 'typestyle/lib';
import { InfoScreen } from '../shared/InfoScreen'


export const SkillMenu: React.StatelessComponent = () =>
	<InfoScreen title="Schwierigkeitsgrad">
		<nav className={menuClass}>
			{appState.task && appState.task.skillLevels.map((l, i) => ([
				<button onClick={() => chooseSkillLevel(i)} key={'skillLevel' + i}>{l.label}</button>,
				<br key={'br' + i}/>
			]))}
		</nav>
	</InfoScreen>


const menuClass = style(screenBase)

