import * as React from 'react';
import { bigFont, colorFontYellow } from '../styles/shared';
import { style, keyframes, classes } from 'typestyle/lib';
import { levelState, bulletCharge, reloadBullets } from '../../state/level';
import { observer } from 'mobx-react';
import Hammer from 'react-hammerjs'
import { TransitionGroup, CSSTransition } from 'react-transition-group';


export const Game: React.StatelessComponent = observer(() => {
	const bullets: any = []
	for (let i = 1; i <= bulletCharge; i++) {
		bullets.push(i <= levelState.bullets
			? (<img draggable={false} onDragStart={() => false} src="img/pfeil_gelb.png" key={'bullet' + i} />)
			: (<img draggable={false} onDragStart={() => false} src="img/pfeil_leer.png" key={'bullet' + i} />)
		)
	}
	return (
		<section className={controlsClass}>
			<div className={bulletsClass}>
				{bullets}
				{levelState.bullets === 0 && (
					<Hammer onTap={reloadBullets}>
						<button className="reloadButton" draggable={false}><img draggable={false} src="img/reload_shots.png" /></button>
					</Hammer>
				)}
			</div>
			<div style={{width: '32.5%'}}>
				<TransitionGroup>
					{levelState.pointAlerts.map(p => (
						<CSSTransition key={'point' + p.id} classNames="point" timeout={1000} enter={false} appear={false}  unmountOnExit>
							<span className={classes(pointAlertClass, p.points < 0 && 'negative')}>{p.points}</span>
						</CSSTransition>
					))}
				</TransitionGroup>
				Punkte: {levelState.points}
			</div>
			<div style={{width: '22.4%'}}>
				Zeit: {Math.floor(levelState.remainingTime / 1000)}
			</div>
		</section>
	)
})


const bulletsClass = style({
	width: '44.9%',
	padding: '0 0.5em',
	textAlign: 'left',

	$nest: {
		'&>img': {
			height: '1.5em'
		},

		'&>.reloadButton': {
			position: 'absolute',
			width: '9em',
			minWidth: 0,
			bottom: 0,
			left: 0,
			margin: 'auto',
			padding: 0,
			border: 0,
			background: 'transparent',
			textAlign: 'center',
			outline: 'none',
			cursor: 'inherit',
			boxShadow: 'none',

			$nest: {
				'&>img': {
					height: '2em'
				}
			}
		}
	}
})


const controlsClass = style(bigFont, {
	position: 'absolute',
	bottom: 0,
	left: 0,
	right: 0,
	width: '100%',
	fontSize: '1.6em',
	color: colorFontYellow,

	$nest: {
		'&>div': {
			position: 'relative',
			display: 'block',
			float: 'left',
		}
	}
})


const pointAnimation = keyframes({
	'20%': {
		transform: 'translate(0, -40%) rotate(10deg) scale(1.3)'
	},
	'100%': {
		transform: 'translate(0, 120%) rotate(360deg) scale(0)'
	}
})


const pointAlertClass = style(bigFont, {
	position: 'absolute',
	display: 'block',
	left: 0,
	right: 0,
	top: '-1em',
	color: '#6f3',
	transformOrigin: 'center center',
	fontSize: '1.2em',
	opacity: 1,
	transition: 'opacity 0.6s',
	transitionDelay: '0.3s',

	$nest: {
		'&.negative': {
			color: 'tomato'
		},
		'&.point-exit': {
			animationName: pointAnimation,
			animationDuration: '1s',
			opacity: 0,
		}
	}
})
