import * as React from 'react';
import { InfoScreen } from '../shared/InfoScreen';
import { appState, openMenu } from '../../state/app';
import { style } from 'typestyle/lib';


export const ViewHighScore: React.StatelessComponent = () =>
	<InfoScreen title="Bestenliste">
		<p><strong>Du bist auf Platz {appState.highscore.rank}</strong></p>
		<div className={containerClass}>
			<table className={tableClass}>
				{appState.highscore.list.map((score, i) => (
					<tr key={'score' + i}><td><div>{i + 1}. Name: {score.name}</div></td><td>{score.score} Punkte</td></tr>
				))}
			</table>
		</div>
		<nav>
			<button onClick={openMenu}>Neues Spiel</button>
		</nav>
	</InfoScreen>

const containerClass = style({
	height: '24em',
	overflow: 'auto'
})

const tableClass = style({
	// display: 'block',
	tableLayout: 'fixed',
	textAlign: 'left',
	position: 'relative',
	maxWidth: '100%',
	width: '100%',
	borderCollapse: 'collapse',
	$nest: {
		'& tr': {
			maxWidth: '100%'
		},
		'& td': {
			padding: '0.2em 0.4em',
			backgroundColor: '#ffefbf'
		},
		'& td:last-child': {
			width: '7.8em',
			textAlign: 'right'
		},
		'& div': {
			overflow: 'hidden',
			whiteSpace: 'nowrap',
			textOverflow: 'ellipsis'
		},
		'tr + tr>td': {
			borderTop: '1px solid brown'
		}
	}
})

