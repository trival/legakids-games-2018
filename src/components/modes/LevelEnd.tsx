import * as React from 'react';
import { observer } from 'mobx-react'
import { levelState } from '../../state/level';
import { beginnLevel, nextLevel, appState, openMenu, enterHighScore } from '../../state/app';
import { InfoScreen } from '../shared/InfoScreen';


export const LevelEnd: React.StatelessComponent = observer(() => {
	const points = levelState.points + appState.totalPoints

	return (
		<InfoScreen title="Deine Punkte">
			<div style={{ fontSize: '1.2em' }}>
				{appState.totalPoints > 0 && [
					`Punkte aus anderen Runden: ${appState.totalPoints}\u00a0Punkte`,
					<br />
				]}
				Treffer in dieser Runde: {levelState.points}&nbsp;Punkte
				<br />
				<p>
					<strong>Gesamt: {points}&nbsp;Punkte</strong>
				</p>
			</div>

			{appState.isLastLevel ? (
				<nav>
					<button onClick={beginnLevel}>
						Noch mal spielen
					</button>
					<button onClick={openMenu}>
						Neues Spiel
					</button>
					<br/>
					<br/>
					{!!navigator.onLine && (
						<button onClick={() => enterHighScore(points)}>
							Bestenliste eintragen
					</button>
					)}
				</nav>
			) : (
					<nav>
						<button onClick={beginnLevel}>
							Noch mal spielen
						</button>
						<button onClick={() => nextLevel(points)}>
							Nächste Runde
						</button>
					</nav>
				)}
		</InfoScreen>
	)
})
