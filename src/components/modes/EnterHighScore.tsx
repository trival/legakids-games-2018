import * as React from 'react';
import { InfoScreen } from '../shared/InfoScreen';
import { openMenu, sendScore } from '../../state/app';


interface State {
	name: string,
	sending: boolean
}


export class EnterHighScore extends React.Component<{}, State> {
	state: State

	constructor(props) {
		super(props)
		this.state = {
			name: '',
			sending: false
		}
		this.send = this.send.bind(this)
		this.input = this.input.bind(this)
	}

	send (e) {
		e.preventDefault()
		if (this.state.name) {
			this.setState({ sending: true })
			sendScore(this.state.name)
		}
	}

	input (e) {
		this.setState({ name: e.target.value })
	}

	render () {
		return (
			<InfoScreen title="Trage deinen Namen ein!">
				<form onSubmit={this.send}>
					<p>Dieser Name erscheint in der Bestenliste:</p>
					<input type="text" disabled={this.state.sending} value={this.state.name} onChange={this.input} />
					<nav>
						<button disabled={this.state.sending} type="submit">Abschicken</button>
						<button onClick={openMenu}>Neues Spiel</button>
					</nav>
				</form>
			</InfoScreen>
		)
	}
}
