import * as React from 'react';
import { preloaderState } from '../../state/preloader';
import { observer } from 'mobx-react';
import { fullAbsolute, bigFont, colorFontYellow } from '../styles/shared';
import { style } from 'typestyle/lib';
import { screenTransitionDuration } from '../../state/app';
import * as css from 'csstips'


export const LoadScreen: React.StatelessComponent = observer(() =>
	<section className={preloadClass}>
		<p>lade... {Math.floor(100 * Math.min(1, (preloaderState.loaded + 1) / preloaderState.total))}%</p>
	</section>
)

const transitionSeconds = screenTransitionDuration / 1000


const preloadClass = style(bigFont, fullAbsolute, css.vertical, css.centerCenter, {
	position: 'relative',
	fontSize: '2em',
	color: colorFontYellow,
	backgroundColor: 'white',
	backgroundImage: 'url(img/splash.jpg)',
	backgroundSize: 'cover',
	backgroundPosition: 'center center',
	zIndex: 100,
	opacity: 1,
	transition: `opacity ${transitionSeconds}s`,

	$nest: {
		'&.screen-exit': {
			opacity: 0
		},

		'& p': {
			position: 'absolute',
			width: '100%',
			top: 'auto',
			left: 0,
			right: 0,
			bottom: '1em'
		}
	}
})

