import * as React from 'react';
import { openMenu } from '../../state/app';
import { InfoScreen } from '../shared/InfoScreen';
import { gameObjects } from '../../data/config';


export const Start: React.StatelessComponent = () =>
	<InfoScreen title={gameObjects.title}>
		{gameObjects.instructions}
		<nav>
			<button onClick={openMenu}>Jetzt spielen</button>
		</nav>
		<footer>
			<img src="img/anleitung_illu2_klein.png" alt=""/>
		</footer>
	</InfoScreen>


