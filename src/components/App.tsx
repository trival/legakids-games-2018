// import DevTools from 'mobx-react-devtools';
import * as React from 'react';
import { Start } from './modes/Start';
import { appState, screenTransitionDuration } from '../state/app';
import { observer } from 'mobx-react';
import { GameMode } from '../types';
import { Game } from './modes/Game';
import { Menu } from './modes/Menu';
import { LoadScreen } from './modes/LoadScreen';
import { LevelEnd } from './modes/LevelEnd';
import { Scene } from './scene/Scene'
import { style, classes, media } from 'typestyle/lib';
import { fullAbsolute, bigFont, colorButtonGreen, sizeLG, sizeXL, sizeMD, sizeXS, sizeSM } from './styles/shared';
import { levelState } from '../state/level';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { preloaderState } from '../state/preloader';
import { EnterHighScore } from './modes/EnterHighScore';
import { ViewHighScore } from './modes/ViewHighScore';
import { SkillMenu } from './modes/SkillMenu';


export const App: React.StatelessComponent = observer(() => {
	let Screen: any = Start

	switch (appState.gameMode) {
		case GameMode.MENU:
			Screen = Menu
			break
		case GameMode.SKILL_LEVEL:
			Screen = SkillMenu
			break
		case GameMode.GAME:
			Screen = Game
			break
		case GameMode.LEVEL_END:
			Screen = LevelEnd
			break
		case GameMode.ENTER_HIGH_SCORE:
			Screen = EnterHighScore
			break
		case GameMode.VIEW_HIGH_SCORE:
			Screen = ViewHighScore
			break
	}

	return (
		<article className={classes(appClass, levelState.active && 'playing')}>
			<Scene />
			<TransitionGroup component={null} >
				{preloaderState.isReady ? (
					<CSSTransition key={appState.gameMode} classNames="screen" timeout={screenTransitionDuration} unmountOnExit>
						<Screen />
					</CSSTransition>
				) : (
					<CSSTransition key="loadScreen" classNames="screen" timeout={screenTransitionDuration} unmountOnExit>
						<LoadScreen />
					</CSSTransition>
				)}
			</TransitionGroup>
			{/* {process.env.NODE_ENV !== 'production' && (
				<DevTools />
			)} */}
		</article>
	)
})


const appClass = style(fullAbsolute, {
	textAlign: 'center',
	'-webkit-user-select': 'none',
	userSelect: 'none',
	fontFamily: 'Amble Regular',
	fontSize: '14px',
	lineHeight: 1.6,
	overflow: 'hidden',

	$nest: {
		'& *': {
			'-webkit-user-select': 'none',
			userSelect: 'none',
		},
		'& h1, & h2, & h3, & h4, & button, & strong': {
			...bigFont,
			lineHeight: 1.2
		},
		'&.playing': {
			// cursor: 'url(img/cursor.png) 48 48, auto'
		},
		'& button': {
			padding: '0.8em 1em',
			margin: '0.4em',
			border: 0,
			minWidth: '12em',
			fontSize: '1.1em',
			color: 'white',
			textAlign: 'center',
			backgroundColor: colorButtonGreen,
			boxShadow: '0 6px 10px rgba(0,0,0,0.3)',
			borderRadius: '0.3em',
			outline: 'none'
		},

	},
},
	media({ minWidth: sizeXS }, {
		fontSize: '16px'
	}),
	media({ minWidth: sizeSM }, {
		fontSize: '18px'
	}),
	media({ minWidth: sizeMD }, {
		fontSize: '20px'
	}),
	media({ minWidth: sizeLG }, {
		fontSize: '22px'
	}),
	media({ minWidth: sizeXL }, {
		fontSize: '24px'
	}),
)

