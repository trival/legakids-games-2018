import { style, keyframes } from "typestyle/lib"
import { NestedCSSProperties } from "typestyle/lib/types";


export const colorInfoBackground = '#f7e4a9'
export const colorFontBlue = '#035d78'
export const colorButtonGreen = '#1d9d37'
export const colorFontYellow = '#f2de01'

export const sizeXS = '570px'
export const sizeSM = '770px'
export const sizeMD = '1040px'
export const sizeLG = '1280px'
export const sizeXL = '1600px'


export const infoScreen: NestedCSSProperties = {
  display: 'inline-block',
  width: '1em',
  height: '1em',
  strokeWidth: 0,
  stroke: 'currentColor',
  fill: 'currentColor'
}


export const infoScreenClass = style(infoScreen)


export const listReset: NestedCSSProperties = {
	listStyle: 'none',
	margin: 0,
	padding: 0
}


export const fullAbsolute: NestedCSSProperties = {
	position: 'absolute',
	top: 0,
	left: 0,
	width: '100%',
	height: '100%',
}


export const screenBase: NestedCSSProperties = {
	position: 'relative'
}


export const gpuTransform: NestedCSSProperties = {
	'-webkit-transform': 'translate3d(0, 0, 0)',
	transform: 'translate3d(0, 0, 0)',
	'-webkit-backface-visibility': 'hidden',
	backfaceVisibility: 'hidden',
	'-webkit-perspective': 1000,
	perspective: 1000
}


export const bigFont: NestedCSSProperties = {
	fontFamily: 'Riffic-Bold',
	letterSpacing: '0.1em'
}


export const spriteAnimationStart = {
	backgroundPosition: '0px center',
	// backgroundRepeat: 'no-repeat',
	backgroundSize: 'auto 100%',
}


export function spriteAnimationKeyframes (steps: number, spriteWidth: number) {
	const firstStep = Math.floor(spriteWidth / steps)
	return keyframes({
		'0%': {
			backgroundPosition: `-${firstStep}px`
		},
		'100%': {
			backgroundPosition: `-${spriteWidth}px`
		}
	})
}


export function spriteAnimationRun (animation: string, duration: number, steps: number) {
	return {
		animationName: animation,
		animationTimingFunction: `steps(${steps - 1})`,
		animationIterationCount: 1,
		animationDuration: duration / 1000 + 's',
		opacity: 0,
		transition: 'opacity 0.1s',
		transitionDelay: (duration - 100) / 1000 + 's'
	}
}
