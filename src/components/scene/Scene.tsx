import * as React from 'react';
import { style } from 'typestyle/lib';
import { fullAbsolute } from '../styles/shared';
import { VisualScene } from './VisualScene'
import * as numbersScene from './InteractionSceneNumbers';
import * as wordsScene from './InteractionSceneWords';

const InteractionScene = window['game'] === 'words'
	? wordsScene.InteractionScene : numbersScene.InteractionScene


export const Scene: React.StatelessComponent = () => {
	return (
		<section className={sceneClass}>
			<InteractionScene></InteractionScene>
			<VisualScene></VisualScene>
		</section>
	)
}


const sceneClass = style(fullAbsolute, {
	overflow: 'hidden',
})
