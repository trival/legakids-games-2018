import * as React from 'react';
import { style, classes, keyframes } from 'typestyle/lib';
import { fullAbsolute, gpuTransform, spriteAnimationStart, spriteAnimationKeyframes, spriteAnimationRun } from '../styles/shared';
import { gameObjects, sharedGameObjects } from '../../data/config';
import { sceneState } from '../../state/scene';
import { observer } from 'mobx-react';
import { Cloud, Lurs } from '../../types';
import { levelState, countdownDuration, zonkDuration } from '../../state/level';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { getMiddlegroundLursId, getForgroundLursId } from '../../utils/helpers';


const cloudPopDuration = 600
const lursAnimationDuration = 900
const shotAnimationDuration = 80 * 7


interface CloudProps {
	cloud: Cloud,
	scale: number
}


export const VisualCloud: React.StatelessComponent<CloudProps> = observer(({ scale, cloud }) => {
	const width = sharedGameObjects.cloud.width + gameObjects.layerMiddle.width
	const style: any = {
		width: `${sharedGameObjects.cloud.width}px`,
		height: `${sharedGameObjects.cloud.height}px`,
		top: 0,
		left: 0,
		transform: `scale(${scale * 1.1}) translate(${width * cloud.x - sharedGameObjects.cloud.width}px, ${(cloud.y * (gameObjects.layerMiddle.height - sharedGameObjects.cloud.height - 300))}px)`
	}
	return (
		<div className={classes(cloudClass, cloud.correct && 'correct')} style={style}>
			<span>{cloud.text}</span>
		</div>
	)
})


interface LursProps {
	lurs?: Lurs,
	scale: number,
	layer: any,
	idFn: Function
}


export const VisualLursLayer: React.StatelessComponent<LursProps> = observer(({ lurs, scale, layer, idFn }) => {
	const hit = lurs && lurs.hit
	return (
		<TransitionGroup component={null}>
			{layer.lurse.map((l, i) => {
				const id = idFn(i)
				const active = lurs !== undefined && lurs.id === id
				if (active) {
					const y = (layer.height - l.bottom - sharedGameObjects.lurs.height * l.scale) * scale
					const x = l.left * scale
					return (
						<CSSTransition classNames="lurs" key={'la' + id} unmountOnExit timeout={lursAnimationDuration}>
							<div className={classes(l.type, lursClass, hit && 'hit')} key={'ll' + id} style={{ transform: `translate(${x}px, ${y}px) scale(${l.scale * scale})` }}></div>
						</CSSTransition>
					)
				}
			}).filter(v => v)}
		</TransitionGroup>
	)
})


export const VisualScene: React.StatelessComponent = observer(() => {
	const translateF2 = -(sceneState.mdRest * sceneState.currentPosition) * sceneState.screenWidth
	const translateF1 = -(sceneState.fgRest * sceneState.currentPosition) * sceneState.screenWidth
	const scale = sceneState.objectScale
	const currentLurs = levelState.activeLurs as Lurs
	const zonk = gameObjects.layerForground.zonk
	return (
		<div className={classes(sceneElements, currentLurs && 'withLurs')}>
			<img className={bgClass} src={'img/' + gameObjects.layerBackground.img} alt="" />

			<div className={mdClass} style={{ transform: `translateX(${translateF2}px)`, width: gameObjects.layerMiddle.width * scale }} >
				<VisualLursLayer idFn={getMiddlegroundLursId} layer={gameObjects.layerMiddle} lurs={currentLurs} scale={scale} />

				<img className={layerImageClass} src={'img/' + gameObjects.layerMiddle.img} alt="" />

				<TransitionGroup enter={false} appear={false} component={null}>
					{levelState.clouds.map(c => (
						<CSSTransition key={'bitmapcloud' + c.id} classNames={{ exit: 'hit', exitActive: 'hit', exitDone: 'hit' } as any} timeout={cloudPopDuration} unmountOnExit>
							<VisualCloud cloud={c} scale={scale}></VisualCloud>
						</CSSTransition>
					))}
				</TransitionGroup>
			</div>

			<div className={fgClass} style={{ transform: `translateX(${translateF1}px)` }}>
				<VisualLursLayer idFn={getForgroundLursId} layer={gameObjects.layerForground} lurs={currentLurs} scale={scale} />

				<img className={layerImageClass} src={'img/' + gameObjects.layerForground.img} alt="" />

				<div className={classes(zonkClass, levelState.zonk > 0 && 'hit')} style={{
					backgroundImage: `url(img/${zonk.img})`,
					width: zonk.width + 'px',
					height: zonk.height + 'px',
					transform: `translate(${(zonk.left - zonk.width * 0.5) * scale}px, ${(zonk.top - zonk.height * 0.5) * scale}px) scale(${scale})`
				}} />

				<TransitionGroup enter={false} appear={false} component={null}>
					{levelState.shots.map(s => (
						<CSSTransition key={'shot' + s.time + '' + s.x + '' + s.y} classNames="shot" timeout={shotAnimationDuration} unmountOnExit>
							<div className={shotClass} style={{ transform: `translate(${s.x - translateF1}px, ${s.y}px)` }}></div>
						</CSSTransition>
					))}
				</TransitionGroup>
			</div>

			{[3, 2, 1]
			.filter(i => i === levelState.countdown)
			.map(i => (
				<div className={classes(countdownClass, 'countdown' + i)} key={'countdown' + i}></div>
			))}

			<TransitionGroup enter={false} appear={false} component={null}>
				{levelState.pointAlerts.filter(p => p.points < 0).map(p => (
					<CSSTransition key={'wrong' + p.id} classNames='wrong' timeout={300} unmountOnExit enter={false} appear={false}>
						<div className={wrongClass}></div>
					</CSSTransition>
				))}
			</TransitionGroup>
		</div>
	)
})


const sceneElements = style(fullAbsolute, {
	...gpuTransform,
	pointerEvents: 'none',
	// opacity: 0.8,
})


const layerImageClass = style({
	position: 'absolute',
	top: 0,
	left: 0,
	height: '100%'
})


const bgClass = style({
	position: 'absolute',
	top: 0,
	left: 0,
	width: '100%',
	height: '100%',
	objectFit: 'cover',
	objectPosition: 'center bottom'
})


const mdClass = style(fullAbsolute, gpuTransform, {
})


const fgClass = style(fullAbsolute, gpuTransform, {
})



const cloudAnimation = spriteAnimationKeyframes(
	sharedGameObjects.cloud.animationSteps,
	sharedGameObjects.cloud.backgroundWidth
)


const cloudClass = style(spriteAnimationStart, {
	position: 'absolute',
	top: 0,
	left: 0,
	fontSize: '39px',
	backgroundImage: `url(img/${sharedGameObjects.cloud.imgWrong})`,
	transformOrigin: '0 0',
	textTransform: 'uppercase',

	$nest: {
		'&.correct': {
			backgroundImage: `url(img/${sharedGameObjects.cloud.imgCorrect})`,
		},
		'&.hit': {
			...spriteAnimationRun(cloudAnimation, cloudPopDuration, sharedGameObjects.cloud.animationSteps),
		},

		'& span': {
			position: 'absolute',
			top: 0,
			bottom: 0,
			left: 0,
			right: 0,
			height: '1em',
			margin: 'auto',
			textDecoration: 'uppercase',
			whiteSpace: 'nowrap'
		},

		'&.hit span': {
			opacity: 0,
			transition: `opacity ${cloudPopDuration / 1000}s`
		},
	}
})


const lursAnimation = spriteAnimationKeyframes(
	sharedGameObjects.lurs.animationSteps,
	sharedGameObjects.lurs.backgroundWidth
)


const lursClass = style({
	position: 'absolute',
	top: 0,
	left: 0,
	width: sharedGameObjects.lurs.width + 'px',
	height: sharedGameObjects.lurs.height + 'px',
	transformOrigin: '0 0',

	$nest: {
		'&::after': {
			...fullAbsolute,
			...spriteAnimationStart,
			display: 'block',
			content: '" "',
			transform: 'translate(0, 0)',
			transition: 'transform ' + (lursAnimationDuration / 1500) + 's',
			transitionTimingFunction: 'cubic-bezier(.3,.6,.7,1.2)',
			transitionDelay: '0s'
		},

		'&.lurs1::after': {
			backgroundImage: `url(img/${sharedGameObjects.lurs.types.lurs1.img})`
		},
		'&.lurs2::after': {
			backgroundImage: `url(img/${sharedGameObjects.lurs.types.lurs2.img})`
		},
		'&.lurs3::after': {
			backgroundImage: `url(img/${sharedGameObjects.lurs.types.lurs3.img})`
		},

		'&.lurs-enter::after': {
			transform: 'translateY(100%)'
		},
		'&.lurs-enter-active::after': {
			transform: 'translateY(0)'
		},
		'&.lurs-exit::after': {
			transform: 'translateY(100%)',
			transitionTimingFunction: 'default',
			transition: 'transform ' + (lursAnimationDuration / 1000) + 's',
		},
		'&.hit.lurs-exit::after': {
			...spriteAnimationRun(lursAnimation, lursAnimationDuration, sharedGameObjects.lurs.animationSteps),
			transform: 'translateY(0)',
		}
	}
})


const zonkSteps = gameObjects.layerForground.zonk.animationSteps || 5

const zonkAnimation = spriteAnimationKeyframes(zonkSteps, gameObjects.layerForground.zonk.width * zonkSteps)

const zonkClass = style(spriteAnimationStart, {
	position: 'absolute',
	top: 0,
	left: 0,
	transformOrigin: '0 0',

	$nest: {
		'&.hit': {
			...spriteAnimationRun(zonkAnimation, zonkDuration, zonkSteps),
			transition: 'none',
			opacity: 1
		}
	}
})


const shotAnimation = spriteAnimationKeyframes(7, 1792)


const shotClass = style({
	position: 'absolute',
	top: 0,
	left: 0,
	width: '256px',
	height: '256px',

	$nest: {
		'&::after': {
			...fullAbsolute,
			...spriteAnimationStart,
			display: 'block',
			content: '" "',
			transform: 'translate(-50%, -50%) scale(0.5)',
			backgroundImage: 'url(img/schuss_map256x256@80ms.png)'
		},
		'&.shot-exit::after': {
			...spriteAnimationRun(shotAnimation, shotAnimationDuration, 7),
		}
	}
})


const countdownAnimation = keyframes({
	'0%': {
		transform: 'scale(0.9)'
	},
	'10%': {
		transform: 'scale(1)'
	},
	'100%': {
		transform: 'scale(0)'
	}
})

const countdownClass = style(fullAbsolute, {
	backgroundSize: 'auto 33%',
	backgroundPosition: 'center center',
	backgroundRepeat: 'no-repeat',
	animationName: countdownAnimation,
	animationDuration: (countdownDuration + 30) / 1000 + 's',
	animationIterationCount: 1,

	$nest: {
		'&.countdown1': {
			backgroundImage: 'url(img/countdown_1.png)'
		},
		'&.countdown2': {
			backgroundImage: 'url(img/countdown_2.png)'
		},
		'&.countdown3': {
			backgroundImage: 'url(img/countdown_3.png)'
		},
	}
})


const wrongAnimation = keyframes({
	'0%': {
		background: [
			'-webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0) 0%,rgba(205,37,37,0.3) 100%)',
			'radial-gradient(ellipse at center, rgba(255,255,255,0) 0%,rgba(205,37,37,0.3) 100%)'
		]
	},
	'33%': {
		background: [
			'-webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0) 0%,rgba(235,107,107,0.2) 100%)',
			'radial-gradient(ellipse at center, rgba(255,255,255,0) 0%,rgba(235,107,107,0.2) 100%)'
		]
	},
	'66%': {
		background: [
			'-webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0) 0%,rgba(255,137,137,0.1) 100%)',
			'radial-gradient(ellipse at center, rgba(255,255,255,0) 0%,rgba(255,137,137,0.1) 100%)'
		]
	},
	'100%': {
		background: [
			'-webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0) 0%,rgba(255,255,255,0) 100%)',
			'radial-gradient(ellipse at center, rgba(255,255,255,0) 0%,rgba(255,255,255,0) 100%)'
		]
	}
})

const wrongClass = style(fullAbsolute, {
	animationName: wrongAnimation,
	animationDuration: '0.3s'
})
