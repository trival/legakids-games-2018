import { GameData } from "./types";

declare module "*.json" {
	const data: GameData;
	export default data;
}

declare global {
	const ActiveXObject: any
}
