import * as React from 'react';
import { LevelData } from '../types';


export const gameObjects = {
	title: 'Zahlenjagd',

	layerBackground: {
		width: 1680,
		height: 1200,
		img: 'numbers/crop_zahlenjagd_BG_01.png'
	},

	layerMiddle: {
		width: 3396,
		height: 1200,
		img: 'numbers/crop_zahlenjagd_BG_02.png',
		lurse: [{
			type: 'lurs3',
			left: 160,
			bottom: 270,
			scale: 0.8,
			points: 250,
			showTime: 3000
		}, {
			type: 'lurs1',
			left: 930,
			bottom: 345,
			scale: 0.7,
			points: 250,
			showTime: 2000
		}]
	},

	layerForground: {
		width: 5009,
		height: 1200,
		img: 'numbers/crop_zahlenjagd_BG_03.png',
		lurse: [{
			type: 'lurs3',
			left: 3790,
			bottom: -30,
			scale: 1.3,
			points: 250,
			showTime: 3000
		}, {
			type: 'lurs2',
			left: 2100,
			bottom: -20,
			scale: 1,
			points: 250,
			showTime: 2000
		}],
		zonk: {
			img: 'numbers/fledermaus_map_300x300.png',
			sound: 'numbers/FX_fledermaus',
			animationSteps: 6,
			left: 3043 + 150,
			top: 295 + 150,
			width: 300,
			height: 300,
			scale: 1,
			points: -100,
		}
	},

	musicLevel1: 'numbers/music_zahlenjagd_Level1',
	musicLevel2: 'numbers/music_zahlenjagd_Level2',
	musicLevel3: 'numbers/music_zahlenjagd_Level3',

	instructions: [
		<p>Jage eine Reihe aus dem kleinen Einmaleins!</p>,
		<p>Für jede Reihe gibt es drei Runden. Blitze mit der Reflektormaschine und ziele dabei auf die Wortwolken. Wenn du eine Zahl aus der richtigen Reihe triffst, bekommst du Punkte. Aber Vorsicht, triffst du eine falsche Zahl, werden dir Punkte abgezogen. Wenn du Lurs blitzt, gibt das natürlich Extra-Punkte! Nach zehn Blitzen musst du die Energie wieder aufladen.</p>,
		<p>Wenn du alle drei Runden einer Einmaleinsreihe geschafft hast, kannst du dich in die Bestenliste eintragen.</p>,
	]
}


export const gameData = {
	gameUrl: 'https://games.legakids.net/zahlenjagd/data/',
	tasks: [{
		label: '2er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write2reihe.php',
		skillLevels: [{
			docName: 'Reihe_2.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: '3er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write3reihe.php',
		skillLevels: [{
			docName: 'Reihe_3.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: '4er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write4reihe.php',
		skillLevels: [{
			docName: 'Reihe_4.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: '5er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write5reihe.php',
		skillLevels: [{
			docName: 'Reihe_5.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: '6er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write6reihe.php',
		skillLevels: [{
			docName: 'Reihe_6.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: '7er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write7reihe.php',
		skillLevels: [{
			docName: 'Reihe_7.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: '8er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write8reihe.php',
		skillLevels: [{
			docName: 'Reihe_8.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: '9er-Reihe',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/zahlenjagd/write9reihe.php',
		skillLevels: [{
			docName: 'Reihe_9.xml',
			levels: [] as LevelData[]
		}]
	}]
}
