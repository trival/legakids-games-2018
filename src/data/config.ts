import * as numbersConfig from './numbers-game'
import * as wordsConfig from './words-game'
import { GameData } from '../types';
import * as Hammer from 'hammerjs'



const config = window['game'] === 'words'
	? wordsConfig : numbersConfig

export const gameData: GameData = config.gameData

export const gameObjects = config.gameObjects

export const hammerOptions = {
	recognizers: {
		swipe: { enable: false },
		press: { enable: false },
		pinch: { enable: false },
		rotate: { enable: false },
		pan: { pointers: 1, direction: Hammer.DIRECTION_HORIZONTAL }
	}
}

export const sharedGameObjects = {
	lurs: {
		width: 512,
		height: 512,
		animationSteps: 9,
		backgroundWidth: 4608,
		types: {
			lurs1: {
				img: 'lurs1_map2.png',
			},
			lurs2: {
				img: 'lurs2_map2.png',
			},
			lurs3: {
				img: 'lurs3_map2.png',
			}
		}
	},

	cloud: {
		width: 512,
		height: 256,
		animationSteps: 5,
		imgCorrect: 'wolke_map2_512x256.png',
		imgWrong: 'wolke_map_falsch_512x256.png',
		backgroundWidth: 2560
	}
}
