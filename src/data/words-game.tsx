import * as React from 'react';
import { LevelData } from '../types';


export const gameObjects = {
	title: 'Wörterjagd',

	layerBackground: {
		width: 1920,
		height: 1200,
		img: 'words/crop_worterjagd_BG01.png'
	},

	layerMiddle: {
		width: 2880,
		height: 1200,
		img: 'words/crop_worterjagd_BG03.png',
		lurse: [{
			type: 'lurs2',
			left: 900,
			bottom: 100,
			scale: 0.7,
			points: 250,
			showTime: 2000
		}]
	},

	layerForground: {
		width: 3840,
		height: 1200,
		img: 'words/crop_worterjagd_BG04_ohneEule.png',
		lurse: [{
			type: 'lurs1',
			left: 2700,
			bottom: 20,
			scale: 1,
			points: 250,
			showTime: 3000
		}, {
			type: 'lurs3',
			left: 500,
			bottom: 100,
			scale: 1,
			points: 250,
			showTime: 3000
		}],
		zonk: {
			img: 'words/eule_map2_150x150.png',
			sound: 'words/FX_eule',
			animationSteps: 5,
			left: 1035,
			top: 295,
			width: 150,
			height: 150,
			scale: 1,
			points: -100,
		}
	},

	musicLevel1: 'words/music_worterjagd_Level1',
	musicLevel2: 'words/music_worterjagd_Level2',
	musicLevel3: 'words/music_worterjagd_Level3',

	instructions: [
		<p>Jage Nomen, Adjektive oder Verben!</p>,
		<p>Für jede Wortart gibt es drei Runden. Blitze mit der Reflektormaschine und ziele dabei auf die Wortwolken. Wenn du ein Wort der richtigen Wortart triffst, bekommst du Punkte. Aber Vorsicht, triffst du ein falsches Wort, werden dir Punkte abgezogen. Wenn du Lurs blitzt, gibt das natürlich Extra-Punkte!</p>,
		<p>Nach zehn Blitzen musst du die Energie wieder aufladen.</p>,
		<p>Wenn du alle drei Runden einer Wortart geschafft hast, kannst du dich in die Bestenliste eintragen.</p>,
	]
}


export const gameData = {
	gameUrl: 'https://games.legakids.net/woerterjagd/data/',
	tasks: [{
		label: 'Nomen Jagen',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/woerterjagd/writeNomen.php',
		skillLevels: [{
			label: 'leicht',
			docName: 'complexity_01/nomen.xml',
			levels: [] as LevelData[]
		}, {
			label: 'mittel',
			docName: 'complexity_02/nomen.xml',
			levels: [] as LevelData[]
		}, {
			label: 'schwer',
			docName: 'complexity_03/nomen.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: 'Adjektive Jagen',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/woerterjagd/writeAdjektive.php',
		skillLevels: [{
			label: 'leicht',
			docName: 'complexity_01/adjektive.xml',
			levels: [] as LevelData[]
		}, {
			label: 'mittel',
			docName: 'complexity_02/adjektive.xml',
			levels: [] as LevelData[]
		}, {
			label: 'schwer',
			docName: 'complexity_03/adjektive.xml',
			levels: [] as LevelData[]
		}]
	},

	{
		label: 'Verben Jagen',
		highScoreUrl: 'https://www.legakids.net/fileadmin/user_upload/Flash/woerterjagd/writeVerben.php',
		skillLevels: [{
			label: 'leicht',
			docName: 'complexity_01/verben.xml',
			levels: [] as LevelData[]
		}, {
			label: 'mittel',
			docName: 'complexity_02/verben.xml',
			levels: [] as LevelData[]
		}, {
			label: 'schwer',
			docName: 'complexity_03/verben.xml',
			levels: [] as LevelData[]
		}]
	}]
}
