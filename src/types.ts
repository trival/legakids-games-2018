export enum GameMode {
	START = 'start',
	MENU = 'menu',
	SKILL_LEVEL = 'skill_level',
	GAME = 'game',
	LEVEL_END = 'level_end',
	ENTER_HIGH_SCORE = 'enter_high_score',
	VIEW_HIGH_SCORE = 'view_high_score'
}


export interface AppState {
	gameMode: GameMode
	level: number
	skillLevel: number
	task?: TaskData
	totalPoints: number
	isLastLevel: boolean,
	highscore: HighScore
}


export interface LevelState {
	active: boolean
	remainingTime: number
	lastFrameTime: number
	points: number
	bullets: number,
	clouds: Cloud[],
	targets: TargetData[],
	nextTarget: number,
	countdown: number,
	hits: number[],
	shots: Shot[],
	pointAlerts: PointAlert[],
	zonk: number
	activeLurs?: Lurs
	timeToLurs: number,
	timeToCloud: number
	cloudPause: number
	endSoundPlayed: boolean

	canShoot: boolean
	isPlaying: boolean
}


export interface SceneState {
	window: {
		width: number,
		height: number,
		ratio: number
	},

	objectScale: number,
	screenWidth: number,
	cloudWidth: number,

	currentPosition: number
	positionDelta: number

	fgToScreen: number,
	mdToScreen: number,
	fgRest: number,
	mdRest: number
}


export class HighScore {
	rank: number = 0
	list: Array<{ name: string, score: string }> = []
}


export interface PreloaderState {
	promises: Promise<any>[]
	total: number
	loaded: number
	isReady: boolean
}


export interface Cloud extends TargetData {
	id: number
	text: string
	x: number
	y: number
	speed: number,
	correct: boolean
}


export interface ScreenEvent {
	x: number,
	y: number,
	processed: boolean
}


export interface Shot extends ScreenEvent {
	time: number
}

export interface PointAlert {
	points: number
	id: number
}

export interface Lurs {
	ttl: number
	hit: boolean
	dead: boolean
	id: string
	points: number
}


export interface PositionEvent {
	center: {
		x: number
		y: number
	},
	deltaX: number,
	velocityX: number
	isFirst: boolean,
	isFinal: boolean,
	pointers: any[]
}


// Game Data

export interface GameData {
	gameUrl: string
	tasks: TaskData[]
}

export interface TaskData {
	label: string
	highScoreUrl: string
	skillLevels: SkillLevelData[]
}

export interface SkillLevelData {
	label?: string
	docName: string
	levels: LevelData[]
}

export interface LevelData {
	options: LevelOptionData[]
}

export interface LevelOptionData {
	targets: TargetData[]
}

export interface TargetData {
	text: string
	correct: boolean
}

