import { observable, action } from 'mobx'
import { AppState, GameMode, TaskData, HighScore } from '../types';
import { startLevel } from './level';
import { fadeOut, sounds } from './audio';
import { xmlToHighscore } from '../utils/helpers';
import 'whatwg-fetch'


export const screenTransitionDuration = 400

const state = observable({
	gameMode: GameMode.START,
	level: 0,
	skillLevel: 0,
	totalPoints: 0,
	highscore: new HighScore(),

	get isLastLevel () {
		return this.task && this.task.skillLevels[this.skillLevel].levels.length === this.level + 1
	}

} as AppState)

export const appState = state


// Actions

export const openMenu = action(() => {
	state.gameMode = GameMode.MENU
	sounds.clickButton.play()
})


export const startTask = action((task: TaskData) => {
	state.level = 0
	state.skillLevel = 0
	state.totalPoints = 0
	state.task = task
	state.highscore = new HighScore()
	if (task.skillLevels.length > 1) {
		state.gameMode = GameMode.SKILL_LEVEL
		sounds.clickButton.play()
	} else {
		beginnLevel()
	}
})


export const chooseSkillLevel = action((n: number) => {
	state.skillLevel = n
	beginnLevel()
})


export const beginnLevel = action(() => {
	state.gameMode = GameMode.GAME
	sounds.clickButton.play()
	if (state.task) {
		startLevel(state.task.skillLevels[state.skillLevel].levels[state.level])
	}
})


export const endLevel = action(() => {
	state.gameMode = GameMode.LEVEL_END
	fadeOut(sounds.musicLevel[state.level])
})


export const nextLevel = action((points: number) => {
	state.totalPoints = points
	state.level++
	beginnLevel()
})


export const enterHighScore = action((points: number) => {
	state.totalPoints = points
	state.gameMode = GameMode.ENTER_HIGH_SCORE
	sounds.clickButton.play()
})


export const sendScore = action((userName: string) => {
	sounds.clickButton.play()
	const data = new FormData()
	data.append('Name', userName)
	data.append('Score', state.totalPoints + '')
	data.append('ListName', 'complexity_0' + (state.skillLevel + 1))

	console.log('sending highscore', userName, state.totalPoints)

	if (state.task) {
		fetch(state.task.highScoreUrl, {
			method: 'POST',
			body: data,
			mode: 'cors'
		})
			.then(res => res.text())
			.then(action((xml: string) => {
				state.highscore = xmlToHighscore(xml)
				if (state.highscore.list.length > 0) {
					state.gameMode = GameMode.VIEW_HIGH_SCORE
				} else {
					openMenu()
				}
			}))
			.catch(e => {
				console.warn(e)
				openMenu()
			})
	}
})
