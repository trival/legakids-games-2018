import { observable, action } from 'mobx'
import { LevelData, PositionEvent, LevelState, PointAlert } from '../types';
import { timeout, onNextTick } from '../utils/scheduler';
import { endLevel as appEndLevel, appState } from './app';
// import { sceneState } from './scene';
import { gameObjects } from '../data/config';
import { getMiddlegroundLursId, getForgroundLursId } from '../utils/helpers';
import { sounds, fadeInMusic } from './audio';
import { moveSceneTransition } from './scene';


const lurse = gameObjects.layerMiddle.lurse.map((l, i) => ({
	...l, id: getMiddlegroundLursId(i)
})).concat(gameObjects.layerForground.lurse.map((l, i) => ({
	...l, id: getForgroundLursId(i)
})))


export const bulletCharge = 10
const roundTime = 1000 * 60 // 60 sec
const speedUpTime = 1000 * 15

const cloudSpeed = 9
const cloudSpeedOffset = 3

const lursTime = roundTime / 10
const lursTimeOffset = 0.2

const cloudPointsMin = 100
const cloudPointsMax = 200
export const countdownDuration = 800

export const zonkDuration = 600


const state = observable({
	active: false,
	remainingTime: roundTime,
	lastFrameTime: 0,
	points: 0,
	timePoints: 0,
	bullets: bulletCharge,
	clouds: [],
	targets: [],
	nextTarget: 0,
	countdown: 0,
	totalTargets: 0,
	hits: [],
	shots: [],
	pointAlerts: [],
	timeToLurs: 0,
	timeToCloud: 0,
	cloudPause: 0,
	activeLurs: undefined,
	endSoundPlayed: false,
	zonk: 0,

	get isPlaying () {
		return !this.countdown && this.active
	},

	get canShoot () {
		return this.isPlaying && this.bullets > 0
	}
} as LevelState)

export const levelState = state


// Actions

const countdown = (ms = 1000) => action(() => {
	state.countdown--
	return timeout(ms)
})


export const startLevel = action((level: LevelData) => {
	const option = level.options[Math.floor(Math.random() * level.options.length)]
	state.targets = option.targets.slice()
	state.nextTarget = 0
	state.points = 0
	state.bullets = bulletCharge
	state.clouds = []
	state.remainingTime = roundTime
	state.active = true
	state.countdown = 4
	state.lastFrameTime = 0
	state.endSoundPlayed = false
	state.cloudPause = roundTime / state.targets.length
	state.zonk = 0
	resetLurs()

	// start countdown
	timeout(countdownDuration)
		.then(() => sounds.countdown.play())
		.then(countdown(countdownDuration))
		.then(countdown(countdownDuration))
		.then(countdown(countdownDuration))
		.then(countdown(0))
		.then(() => {
			onNextTick(tick)
			fadeInMusic(sounds.musicLevel[appState.level])
		})
})


export const endLevel = action(() => {
	state.active = false
	state.clouds = []
	resetLurs()
	appEndLevel()
})


export const hitCloud = action((id: number) => {
	if (state.canShoot) {
		state.hits.push(id)
	}
})


export const hitLurs = action(() => {
	if (state.canShoot && state.activeLurs) {
		state.activeLurs.hit = true
		sounds.lursHit.play()
	}
})


export const reloadBullets = action(() => {
	onNextTick(() => {
		state.bullets = bulletCharge
		sounds.reload.play()
	}, 'reload')
})


export const shoot = action((e: PositionEvent) => {
	if (state.isPlaying) {
		onNextTick(() => {
			if (state.bullets > 0) {
				state.shots.push({
					...e.center,
					processed: false,
					time: state.remainingTime
				})
				state.bullets--
				sounds.shot.play()

				const moveDistance = 0.09 * ((e.center.x / window.innerWidth) - 0.5)
				moveSceneTransition(moveDistance)
			} else {
				sounds.shotEmpty.play()
			}
		}, 'shoot')
	}
})


export const shootZonk = action(() => {
	if (state.zonk === 0 && state.canShoot) {
		onNextTick(() => {
			const points = gameObjects.layerForground.zonk.points
			state.zonk = zonkDuration
			state.pointAlerts.push(createPointAlert(points))
			state.points += points
			sounds.zonk.play()
		}, 'zonk')
	}
})


// main game loop

function tick (time) {
	// frame time

	let tpf = 0

	if (state.lastFrameTime) {
		tpf = time - state.lastFrameTime
		state.remainingTime -= tpf

		// level end condition
		if (state.remainingTime < 0) {
			state.remainingTime = 0
			state.active = false
		}
	}

	if (!state.isPlaying && !state.shots.length) {
			endLevel()
			return
		}

	state.lastFrameTime = time
	state.pointAlerts = []

	// display shot animation

	state.shots = state.shots.filter(s => !s.processed)
	for (const shot of state.shots) {
		shot.processed = true
	}

	// cloud hits

	if (state.hits.length) {
		for (const id of state.hits) {
			const cloudIdx = state.clouds.findIndex(c => c.id === id)
			const cloud = state.clouds[cloudIdx]

			// calculate points, by the distance the cloud already covered
			let step = (cloud.speed > 0 ? 1 - cloud.x : cloud.x + 0.1) // + 0.1 because of cloud scaling to 1.1, so distance on the right is bigger
			step = step * 1.2 - 0.2
			step = Math.min(1, Math.max(0, step))
			let points = lerp(cloudPointsMin, cloudPointsMax, step)
			points = Math.floor(points / 10) * 10

			if (cloud.correct) {
				state.points += points
				sounds.cloudHitCorrect.play()
				state.timeToCloud -= state.cloudPause / 2
				state.pointAlerts.push(createPointAlert(points))
			} else {
				state.points -= points
				sounds.cloudHitFalse.play()
				state.timeToCloud += state.cloudPause / 2
				state.pointAlerts.push(createPointAlert(-points))
			}
			state.clouds.splice(cloudIdx, 1)
		}
		state.hits = []
	}

	// new clouds generation

	if (state.timeToCloud > 0) {
		state.timeToCloud -= tpf
	} else if (state.targets.length > 0) {
		state.clouds.push(createCloud())
		if (state.remainingTime < speedUpTime) {
			state.timeToCloud = state.cloudPause / 1.5
		} else {
			state.timeToCloud = state.cloudPause
		}
	}

	// cloud movement

	for (const cloud of state.clouds) {
		cloud.x += (cloud.speed * (tpf / 100)) / gameObjects.layerMiddle.width
	}
	state.clouds = state.clouds.filter(c => c.x >= 0 && c.x <= 1)

	// lurs appearance

	if (state.timeToLurs > 0) {
		state.timeToLurs -= tpf

	} else if (!state.activeLurs) {
		const randomLurs = lurse[Math.floor(Math.random() * lurse.length)]
		state.activeLurs = {
			id: randomLurs.id,
			hit: false,
			dead: false,
			ttl: randomLurs.showTime,
			points: randomLurs.points,
		}
		sounds.lursAppears.play()

	} else if (state.activeLurs.dead) {
		state.points += state.activeLurs.points
		state.pointAlerts.push(createPointAlert(state.activeLurs.points))
		resetLurs()
	} else if (state.activeLurs.hit) {
		state.activeLurs.dead = true
	} else if (state.activeLurs.ttl > 0) {
		state.activeLurs.ttl -= tpf
	} else {
		resetLurs()
	}

	// end sound

	if (state.remainingTime < 10000 && !state.endSoundPlayed) {
		sounds.beforeEndRound.play()
		state.endSoundPlayed = true
	}

	// zonk

	if (state.zonk > 0) {
		state.zonk -= tpf
		if (state.zonk < 0) {
			state.zonk = 0
		}
	}

	onNextTick(tick)
}


// helper functions

function createCloud () {
	const id = state.nextTarget * roundTime + state.remainingTime
	const target = state.targets[state.nextTarget]
	state.nextTarget = (state.nextTarget + 1) % state.targets.length
	const direction = Math.sign(Math.random() - 0.5)
	let speed = direction * (cloudSpeed + (Math.random() - 0.5) * 2 * cloudSpeedOffset)
	if (state.remainingTime < speedUpTime) {
		speed *= 1.2
	}
	return {
		...target,
		id,
		speed,
		x: speed < 0 ? 1 : 0,
		y: Math.random(),
	}
}


function resetLurs () {
	state.timeToLurs = lursTime + (Math.random() * 2 - 1) * lursTimeOffset
	state.activeLurs = undefined
}


let pointAlertCounter = 0
function createPointAlert (points): PointAlert {
	return {
		points,
		id: pointAlertCounter++
	}
}


function lerp (min, max, step) {
	return min + (max - min) * step
}
