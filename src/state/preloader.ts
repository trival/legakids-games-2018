import { PreloaderState, SkillLevelData } from "../types";
import { observable, action } from "mobx";
import { onNextTick, timeout } from "../utils/scheduler";
import { sharedGameObjects, gameObjects, gameData } from "../data/config";
import { xmlToLevelData } from "../utils/helpers";


const state = observable({
	promises: [timeout(4000)],
	loaded: 0,
	isReady: false,

	get total () {
		return this.promises.length
	},
} as PreloaderState)

export const preloaderState = state


// ===== Actions =====

export const registerAssetPromise = action((p: Promise<any>) => {
	state.promises.push(p)
	p.then(action(() => state.loaded++))
})


// ===== Load images =====

const loadImage = (url) => {
	registerAssetPromise(new Promise(res => {
		const img = new Image()
		img.onload = res
		img.src = 'img/' + url
	}))
}


loadImage('splash.jpg')
loadImage('banner_links.png')
loadImage('banner_mitte.png')
loadImage('banner_rechts.png')
loadImage('countdown_1.png')
loadImage('countdown_2.png')
loadImage('countdown_3.png')
// loadImage('cursor.png')
// loadImage('highscore.png')
// loadImage('logo.png')
// loadImage('naechsteRunde.png')
// loadImage('nochmal.png')
loadImage('pfeil_gelb.png')
loadImage('pfeil_leer.png')
loadImage('reload_shots.png')
loadImage('schuss_map256x256@80ms.png')
loadImage(sharedGameObjects.lurs.types.lurs1.img)
loadImage(sharedGameObjects.lurs.types.lurs2.img)
loadImage(sharedGameObjects.lurs.types.lurs3.img)
loadImage(sharedGameObjects.cloud.imgWrong)
loadImage(sharedGameObjects.cloud.imgCorrect)
loadImage(gameObjects.layerBackground.img)
loadImage(gameObjects.layerMiddle.img)
loadImage(gameObjects.layerForground.img)


// ===== Load game data =====

function loadGameData (skillLevel: SkillLevelData) {
	registerAssetPromise(fetch(gameData.gameUrl + '' + skillLevel.docName)
		.then(res => res.text())
		.then(text => {
			if (!text) throw new Error("no text received")
			return text
		})
		.catch(e => {
			console.warn('remote data could not be loaded', skillLevel, e)
			return fetch('data/' + skillLevel.docName)
				.then(res => res.text())
				.then(text => {
					if (!text) throw new Error("no text received")
					return text
				})
				.catch(e => {
					console.warn('local data could not be loaded', skillLevel, e)
					return ''
				})
		})
		.then(text => {
			if (text) {
				skillLevel.levels = xmlToLevelData(text)
			}
		})
		.catch(e => {
			console.log('xml could not be parsed to LevelData', e)
		})
	)
}


gameData.tasks.forEach(t => {
	t.skillLevels.forEach(loadGameData)
})


// ===== start preloading =====

onNextTick(() => Promise.all(state.promises)
	.then(action(() => state.isReady = true)))
	// .then(() => console.log('game loaded', gameData)))
