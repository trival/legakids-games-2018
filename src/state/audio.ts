import { Howl } from 'howler';
import { registerAssetPromise } from './preloader';
import { gameObjects } from '../data/config';


const musicVolume = 0.6
const soundFadeTime = 700


function fileNameToSrc (s) {
	return ['audio/' + s + '.mp3', 'audio/' + s + '.ogg']
}


function loadSound (filename: string, options: any = {}) {
	let loaded
	registerAssetPromise(new Promise(res => loaded = res ))

	return new Howl({
		...options,
		src: fileNameToSrc(filename),
		onload: () => {
			loaded()
		},
		onloaderror: (...args) => {
			console.warn('error loading sound.', args)
			loaded()
		}
	})
}


export const sounds = {
	beforeEndRound: loadSound('FX_10sek_vor_ende'),
	zonk: loadSound(gameObjects.layerForground.zonk.sound),
	lursAppears: loadSound('FX_Lurs_erscheint'),
	lursHit: loadSound('FX_Lurs_hit'),
	reload: loadSound('FX_reload2'),
	shotEmpty: loadSound('FX_schuss_leer2'),
	shot: loadSound('FX_schuss2'),
	cloudHitFalse: loadSound('FX_wolke_falsch2'),
	cloudHitCorrect: loadSound('FX_wolke_richtig2'),
	clickButton: loadSound('FX_menuewechsel'),
	countdown: loadSound('FX_spielstart'),
	musicLevel: [
		loadSound(gameObjects.musicLevel1, {loop: true}),
		loadSound(gameObjects.musicLevel2, {loop: true}),
		loadSound(gameObjects.musicLevel3, {loop: true})
	]
}


export function fadeInMusic(sound) {
	if (sound.playing()) {
		sound.stop()
	}
	sound.volume(0)
	sound.play()
	sound.fade(0, musicVolume, soundFadeTime)
}

export function fadeOut(sound) {
	if (!sound.playing()) {
		return
	}

	sound.fade(sound.volume(), 0, soundFadeTime)
	setTimeout(() => sound.stop(), soundFadeTime)
}

