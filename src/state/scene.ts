import { SceneState, PositionEvent } from "../types";
import { observable, action } from "mobx";
import { gameObjects, sharedGameObjects } from "../data/config";
import { onNextTick } from "../utils/scheduler";
import { levelState } from "./level";


export const backgroundRatio =
	gameObjects.layerBackground.height / gameObjects.layerBackground.width

export const middleRatio =
	gameObjects.layerMiddle.height / gameObjects.layerMiddle.width

export const forgroundRatio =
	gameObjects.layerForground.height / gameObjects.layerForground.width


const state = observable({
	active: false,
	window: {
		width: window.innerWidth,
		height: window.innerHeight,
		get ratio () {
			return this.height / this.width
		}
	},

	currentPosition: 0.5,
	positionDelta: 0,

	get objectScale () {
		return this.window.height / gameObjects.layerForground.height
	},
	get fgToScreen () {
		return this.window.ratio / forgroundRatio
	},
	get mdToScreen () {
		return this.window.ratio / middleRatio
	},
	get fgRest () {
		return (this.fgToScreen - 1) / this.fgToScreen
	},
	get mdRest () {
		return (this.mdToScreen - 1) / this.fgToScreen
	},
	get screenWidth () {
		return this.window.width * this.fgToScreen
	},
	get cloudWidth () {
		return sharedGameObjects.cloud.width / gameObjects.layerForground.width * this.screenWidth
	}
} as SceneState)

export const sceneState = state


// Action

export const resetWindowSize = action(() => {
	onNextTick(function updateScreenSize () {
		state.window.width = window.innerWidth
		state.window.height = window.innerHeight
	})
})


let dragOrigin = 0

export function resetPan() {
	dragOrigin = state.currentPosition
}

export const updateCurrentPosition = action((pos: PositionEvent) => {
	if (levelState.active) {
		if (pos.isFinal) {
			fadeoutScroll(pos.velocityX)
		}
		onNextTick(function updateCurrentScenePos () {
			state.currentPosition = dragOrigin - pos.deltaX / state.window.width
			if (state.currentPosition > 1) state.currentPosition = 1
			if (state.currentPosition < 0) state.currentPosition = 0
		})
	}
})


const transitionDuration = 1500


export function fadeoutScroll (speed) {
	let lastFrameTime = 0
	const sign = -Math.sign(speed)
	speed = Math.pow(Math.abs(speed), 0.5)
	function run () {
		onNextTick(time => {
			let tpf = 0
			if (lastFrameTime) {
				tpf = time - lastFrameTime
			}
			lastFrameTime = time
			speed -= tpf / 200
			if (speed <= 0) {
				return
			}
			const delta = speed * (tpf / 4000)
			state.positionDelta += delta * sign
			updatePosition()
			run()
		})
	}
	run()
}


export function moveSceneTransition (distance) {
	let previousDistance = 0
	let currentTime = 0
	let lastFrameTime = 0
	function run () {
		onNextTick(time => {
			let tpf = 0
			if (lastFrameTime) {
				tpf = time - lastFrameTime
			}
			lastFrameTime = time
			currentTime += tpf
			if (currentTime > transitionDuration) {
				return
			}
			const phase = currentTime / transitionDuration
			const status = 1 - Math.pow((Math.cos(phase * Math.PI) / 2 + 0.5), 2)
			const currentDistance = distance * status
			const currentPart = currentDistance - previousDistance
			previousDistance = currentDistance
			state.positionDelta += currentPart
			updatePosition()
			run()
		})
	}
	run()
}


export const updatePosition = action(function () {
	onNextTick(function updatePosition() {
		state.currentPosition += state.positionDelta
		state.positionDelta = 0
		if (state.currentPosition > 1) state.currentPosition = 1
		if (state.currentPosition < 0) state.currentPosition = 0
	})
})


// Events

window.addEventListener('resize', resetWindowSize)
