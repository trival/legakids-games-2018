import { HighScore, LevelData } from "../types";


export function getMiddlegroundLursId (index: number) {
	return 'mgl' + index
}

export function getForgroundLursId (index: number) {
	return 'fgl' + index
}


export function parseXML (xml: string): Document {
	let xmlDoc: Document

	if ((window as any).DOMParser) {
		const parser = new DOMParser();
		xmlDoc = parser.parseFromString(xml, "text/xml");

		// Internet Explorer
	} else {
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		(xmlDoc as any).async = false;
		(xmlDoc as any).loadXML(xml);
	}

	return xmlDoc
}


export function xmlToHighscore (xml: string): HighScore {
	const xmlDoc = parseXML(xml)
	const score = {
		rank: 0,
		list: []
	} as HighScore

	const rankNode = xmlDoc.querySelector('rank')
	if (rankNode && rankNode.textContent) {
		score.rank = parseInt(rankNode.textContent)
	}

	const scores = xmlDoc.querySelectorAll('score')
	for (let i = 0; i < scores.length; i++) {
		const node = scores[i]
		score.list.push({
			name: node.getAttribute('attribute') as string,
			score: node.textContent as string
		})
	}
	return score
}


export function xmlToLevelData (xml: string): LevelData[] {
	const xmlDoc = parseXML(xml)
	const level1 = xmlDoc.querySelector('Level1')
	const level2 = xmlDoc.querySelector('Level2')
	const level3 = xmlDoc.querySelector('Level3')

	return [level1, level2, level3]
		.filter(l => l)
		.map(levelDoc => {
			const l = levelDoc as Element
			const opt1 = l.querySelector('Random1')
			const opt2 = l.querySelector('Random2')
			const opt3 = l.querySelector('Random3')

			return {
				options: [opt1, opt2, opt3]
					.filter(o => o)
					.map(opt => {
						const o = opt as Element

						const targetEls = o.querySelectorAll('Eintrag')
						return {
							targets: Array.prototype.slice.call(targetEls)
								.map((el: Element) => ({
									text: el.textContent && el.textContent.trim().toUpperCase(),
									correct: el.getAttribute('attribute') === 'true'
								}))
						}
					})
			}
		})
}
