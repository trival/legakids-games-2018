import { action } from "mobx";


// onNextTick
// schedule actions on next animation frame

let updates: {[id: string]: Function} = {}

let willUpdate = false

let counter = 0
let isProcessing = false


const processUpdates = action(function processUpdates (time: number) {
	isProcessing = true
	for (const id in updates) {
		updates[id](time)
	}
	isProcessing = false
	willUpdate = false
	updates = {}
})


export function onNextTick(fn: Function, id?: string) {
	if (isProcessing) {
		setTimeout(() => onNextTick(fn, id), 0)
		return
	}

	if (id) {
		updates[id] = fn
	}	else if (fn.name) {
		updates[fn.name] = fn
	} else {
		updates[counter++] = fn
	}

	if (!willUpdate) {
		requestAnimationFrame(processUpdates)
		willUpdate = true
	}
}


// timeout
// promised based timeout

export function timeout(ms: number) {
	return new Promise(res => {
		setTimeout(res, ms)
	})
}
