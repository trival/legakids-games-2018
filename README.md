# Legakids Zahlen und Wörter Spiele

Implementierung als Web- und Mobile-App.

## Anleitung zur Entwicklung

```
yarn
yarn dev
```

Die Spiele laufen dann auf http://localhost:3000/woerterjagd/ und http://localhost:3000/zahlenjagd/

## Anleitung zur Veröffentlichung

```
yarn
yarn build
```

Die beiden Spiele liegen dann separat in den Ordnern `dist/woerterjagd` und `dist/zahlenagd`

Dort liegen auch die Konfigurationsdateien für Phonegap Build. In diesen muss die Version hochgezählt werden.
Die Ordner können dann in Phonegap Build als ZIP-Ordner hochgeladen werden.

## Urheberrechte

Artwork: (c) 2018 Jakob Weyde, House of Creatures

Code: (c) 2018 Thomas Gorny
