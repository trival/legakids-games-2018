var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    zahlenjagd: './src/indexNumbers',
    woerterjagd: './src/indexWords'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name]/bundle.js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx']
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: [
        {
          loader: "ts-loader"
        },
      ],
    },
    {
      test: /\.css$/,
      use: [
        { loader: "style-loader" },
        { loader: "css-loader" }
      ]
    }]
  }
};
